﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<Level> _levels;
    [SerializeField] private UIGameScreen _uIgameScreen = null;
    private GameObject _levelObject = null;
    [SerializeField] private GameObject _startScreen = null;
    [SerializeField] private GameObject _winScreen = null;
    [SerializeField] private GameObject _gameScreen = null;
    [SerializeField] private GameObject _levelScreen = null;
    [SerializeField] private Text _timePlayText = null;
    [SerializeField] private Text _resultTimePlay = null;

    [Header("References")]
    [SerializeField] private AdsManager _adsManager = null;

    private DateTime _timePlay = new DateTime(0,0);
    private bool _onPlayLevel = false;

    public void OpenLevels()
    {
        _startScreen.SetActive(false);
        _winScreen.SetActive(false);
        _levelScreen.SetActive(true);
    }
    
    public void StartGame(int index)
    {
        _gameScreen.SetActive(true);
        _levelScreen.SetActive(false);
        InstantiateLevel(index);
    }

    public void HomeGame()
    {
        _startScreen.SetActive(true);
        _winScreen.SetActive(false);
    }

    private void InstantiateLevel(int index)
    {
        if (_levelObject != null)
        {
            Destroy(_levelObject);
            _levelObject = null;
        }
        if (index >= _levels.Count)
        {
            return;
        }
        _levelObject = Instantiate(_levels[index].gameObject, transform);
        Level level = _levelObject.GetComponent<Level>();
        level.Initialize(_gameScreen);
        _uIgameScreen.Initialize(level);
        level.OnLevelComplete = OnLevelComplete;
        _onPlayLevel = true;
        StartCoroutine(TimePlay());
    }
    
    private void OnLevelComplete()
    {
        _onPlayLevel = false;
        GameAnalyticsSDK.GameAnalytics.NewDesignEvent("Level complete", 1);
        _winScreen.SetActive(true);
        _gameScreen.SetActive(false);
        _resultTimePlay.text = _timePlay.ToString("mm:ss");
        _timePlay = new DateTime(0,0);
        _adsManager.ShowInterstitial();
    }
    
    private void Start()
    {
        GameAnalyticsSDK.GameAnalytics.Initialize();
        _adsManager.Initialize();
    }

    private void Update()
    {
        if (_onPlayLevel)
        {
            _timePlayText.text = _timePlay.ToString("mm:ss");
        }
    }

    private IEnumerator TimePlay()
    {
        while (_onPlayLevel)
        {
            _timePlay = _timePlay.AddSeconds(1);
            yield return new WaitForSeconds(1);
        }
        
    }
}
