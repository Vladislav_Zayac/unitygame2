﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameScreen : MonoBehaviour
{
    [SerializeField] private RectTransform _content = null;
    [SerializeField] private GameObject _itemPrefab = null;
    private List<GameObject> uiItems = new List<GameObject>();

    public void Initialize(Level level)
    {
        Dictionary<string, ItemData> items = level.GetItemsData();

        foreach(string key in items.Keys)
        {
            GameObject newItem = Instantiate(_itemPrefab, _content);
            newItem.GetComponent<UIItemController>().Initialize(items[key].ItemSprite, items[key].Amount);
            uiItems.Add(newItem);
            level.OnDecreaseUIItem = OnDescrease;
        }
    }
    public void OnDescrease(ItemData itemData)
    {
        foreach(var i in uiItems)
        {
            if (i.GetComponent<UIItemController>().GetImage() == itemData.ItemSprite)
            {
                i.GetComponent<UIItemController>().Descrease();
            }
        }
    }
}
