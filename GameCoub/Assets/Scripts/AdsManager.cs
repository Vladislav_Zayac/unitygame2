﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdsManager : MonoBehaviour
{
    [SerializeField] private string _androidKey = "85460dcd";
    [SerializeField] private Text text = null;
    private bool _isInterstitialReady;

    public void Initialize()
    {
        IronSource.Agent.init(_androidKey, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.INTERSTITIAL, IronSourceAdUnits.OFFERWALL, IronSourceAdUnits.BANNER);
        IronSource.Agent.validateIntegration();
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
        IronSource.Agent.showInterstitial();
    }

    private void OnEnable()
    {
        IronSourceEvents.onInterstitialAdReadyEvent += OnInterstitialLoaded;
        IronSourceEvents.onInterstitialAdLoadFailedEvent += OnLoadInterstitialFailed;
        IronSourceEvents.onInterstitialAdShowSucceededEvent += OnInterstitialShowSuccess;
        IronSourceEvents.onInterstitialAdClosedEvent += OnInterstitialClosed;

        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
    }

    private void OnDisable()
    {
        IronSourceEvents.onInterstitialAdLoadFailedEvent -= OnLoadInterstitialFailed;
        IronSourceEvents.onInterstitialAdShowSucceededEvent -= OnInterstitialShowSuccess;
        IronSourceEvents.onInterstitialAdReadyEvent -= OnInterstitialLoaded;
        IronSourceEvents.onInterstitialAdClosedEvent -= OnInterstitialClosed;

        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent -= RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent -= RewardedVideoAdShowFailedEvent;
    }

    private void RewardedVideoAvailabilityChangedEvent(bool available)
    {
        bool rewardedVideoAvailability = available;
    }

    private void RewardedVideoAdShowFailedEvent(IronSourceError error)
    {
        Debug.Log($"RewardedVideo load failed! Reason - {error.getDescription()}");
    }

    private void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)//добавить вознагрождение
    {
        text.text = "Bonus";
    }

    public void ShowRewardedVideo()
    {
        IronSource.Agent.setConsent(true);
        IronSource.Agent.showRewardedVideo();
    }




    private void OnInterstitialLoaded()
    {
        _isInterstitialReady = true;
    }

    private void OnInterstitialClosed()
    {
        IronSource.Agent.loadInterstitial();
    }

    private void OnLoadInterstitialFailed(IronSourceError error)
    {
        Debug.Log($"Interstitial load failed! Reason - {error.getDescription()}");
    }

    private void OnInterstitialShowSuccess()
    {
        _isInterstitialReady = false;
    }

    public void ShowInterstitial()
    {
        if (_isInterstitialReady)
        {
            IronSource.Agent.showInterstitial();
        }
    }
}
