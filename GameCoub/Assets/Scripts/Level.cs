﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    private List<Item> _gameItems;
    private Dictionary<string, ItemData> _itemData = new Dictionary<string, ItemData>();
    public System.Action<ItemData> OnDecreaseUIItem;
    public delegate void OnComplete();
    public OnComplete OnLevelComplete;
    private GameObject _contentGameScreen;

    public void Initialize(GameObject gameScreen)
    {
        _contentGameScreen = gameScreen;
        _gameItems = new List<Item>(GetComponentsInChildren<Item>());
        foreach( var i in _gameItems)
        {
            i.Initislized();
            i.OnFind += OnFindAction;
            if (_itemData.ContainsKey(i.Name) != true)
            {
                _itemData.Add(i.Name, new ItemData(i.ItemSprite,1));
            }
            else
            {
                _itemData[i.Name].Amount++;
            }
        }
        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Start, "Initialise level");
    }

    public Dictionary<string,ItemData> GetItemsData()
    {
        return _itemData;
    }

    private void OnFindAction(Item item)
    {
        _gameItems.Remove(item); 
        item.OnFind -= OnFindAction;
        OnDecreaseUIItem?.Invoke(_itemData[item.Name]);
        if (_gameItems.Count == 0)
        {
            OnLevelComplete?.Invoke();
            Destroy(gameObject);
        }
        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, "Find object");
    }
}
