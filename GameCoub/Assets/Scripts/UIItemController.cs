﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItemController : MonoBehaviour
{
    [SerializeField] private Text _countText = null;
    [SerializeField] private Image _image = null;

    private int _count = 0;

    public void Initialize(Sprite sprite, int count)
    {
        _image.sprite = sprite;
        _count = count;
        _countText.text = count.ToString();
    }

    public void Descrease()
    {
        _count--;
        if (_count == 0)
            gameObject.SetActive(false);
        else
            _countText.text = _count.ToString();
        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, "Decrease object");
    }
    public Sprite GetImage()
    {
        return _image.sprite;
    }
}
