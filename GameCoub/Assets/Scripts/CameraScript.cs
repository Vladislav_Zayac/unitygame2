﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] private Vector2 _resolution;
    private void Awake()
    {
        UnityEngine.Camera.main.orthographicSize *= (_resolution.x / _resolution.y) / UnityEngine.Camera.main.aspect;
    }
}
