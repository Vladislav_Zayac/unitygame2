﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Item : MonoBehaviour
{
    [SerializeField] private string _name = string.Empty;

    [Header("Settings")]
    [SerializeField] private float _scaleMultiplier;
    [SerializeField] private float _durationScale;
    [SerializeField] private float _disapearTime;

    private SpriteRenderer _spriteRenderer;
    private bool OnClick = true;

    public System.Action<Item> OnFind;
    public string Name { get { return _name; } set { _name = value; } }
    public Sprite ItemSprite => _spriteRenderer.sprite;

    public void Initislized()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private Coroutine coroutine = null;

    private void OnMouseDown()
    {
        if (OnClick)
        {
            OnClick = false;
            coroutine = StartCoroutine(Scaling());
        }
    }

    private IEnumerator Scaling()
    {
        Vector3 startScale = transform.localScale;
        float currentMultiplier = 1f;
        float speed = (_scaleMultiplier - currentMultiplier) / _durationScale;

        while (currentMultiplier<_scaleMultiplier)
        {
            Vector3 currenScale = startScale * currentMultiplier;
            currentMultiplier += speed * Time.deltaTime;
            transform.localScale = currenScale;

            yield return null;
        }
        yield return new WaitForSeconds(1f);

        float currentAlpha = 1f;
        float alphaSpeed = 1f/_disapearTime;

        while (currentAlpha > 0f)
        {
            Color color = _spriteRenderer.color;
            color.a = currentAlpha;
            currentAlpha -= alphaSpeed * Time.deltaTime;
            _spriteRenderer.color = color;
            yield return null;
        }

        gameObject.SetActive(false);
        OnFind?.Invoke(this);
    }

}
